# Account Calculator

Running the example

```
cd PATH_TO_THIS_DIRECTORY
python account-calculator.py
```

Expected output (What I get on my machine)

```
 ~/code/financial   master  python account-calculator.py
Users that owe penalties:
149: $4
13: $4
15: $4
131: $4
118: $4
154: $12
18: $4
53: $12
92: $8
9: $12
43: $12
105: $8
126: $8
200: $4
160: $4
162: $12
21: $12
176: $12
14: $8
158: $8
6: $4
111: $12
106: $12
125: $8
188: $8
74: $8
124: $4
95: $12
78: $12
114: $8
84: $12
72: $4
138: $8
47: $8
113: $8
184: $12
112: $8
194: $12
35: $12
199: $12
147: $12
31: $12
90: $4
82: $12
January user who deposited the most is User 19, who deposited $2466
February user who deposited the most is User 17, who deposited $2969
March user who deposited the most is User 131, who deposited $3002
January user who made the most transactions is User 160, who made 7
February user who made the most transactions is User 105, who made 7
March user who made the most transactions is User 112, who made 6
```