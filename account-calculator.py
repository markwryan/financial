import csv

# Open and read a csv file as a list
def read_csv(filename):
    with open(filename) as csv_file:
        reader = csv.reader(csv_file)
        return list(reader)


# Load a dictionary of key user_id and value an array of transactions -- basically sort transactions by users
def load_transactions(filename):
    transactions = {}
    # Skip first line, as it is column headers, not data
    data = read_csv(filename)[1:]
    for entry in data:
        transaction = {
            'date': entry[0],
            'user_id': entry[1],
            'amount': int(entry[2])
        }
        # User already exists
        if entry[1] in transactions:
            transactions[entry[1]].append(transaction)
        else:
            transactions[entry[1]] = [transaction]
    return transactions


# Load a list of users
def load_users(filename):
    # Skip first line, as it is column headers, not data
    data = read_csv(filename)[1:]
    users = []
    for entry in data:
        user = {
            'id': entry[0],
            'balance': int(entry[1]),
            'program': entry[2]
        }
        users.append(user)
    return users


# Check what program and call correct one
def calculate_penalties(user, transactions):
    program = user['program']
    if program == 1:
        return program1(user, transactions)
    return program2(user, transactions)


# Total deposit >= 300
# Total transactions >= 5
# Balance >= 1200
def program1(user, transactions):
    # No transactions this month
    if user['id'] not in transactions:
        return 8
    user_transactions = transactions[user['id']]
    # More than 5 transactions
    if len(user_transactions) > 5:
        return 0
    total_deposited = 0
    for transaction in user_transactions:
        amount = transaction['amount']
        total_deposited += amount
        user['balance'] += amount
    # Deposit total or balance is sufficient
    if total_deposited >= 300 or user['balance'] >= 1200:
        return 0
    # No transactions, less that 300 deposited, balance under 1200
    return 8


# Total deposit >= 800
# Total transactions >= 1
# Balance >= 5000
def program2(user, transactions):
    # No transactions this month
    if user['id'] not in transactions:
        return 4
    user_transactions = transactions[user['id']]
    total_deposited = 0
    for transaction in user_transactions:
        amount = transaction['amount']
        total_deposited += amount
        user['balance'] += amount
    # Deposit total or balance is sufficient
    if total_deposited >= 800 or user['balance'] >= 5000:
        return 0
    # No transactions, less that 800 deposited, balance under 5000
    return 4


# Get user with most deposits per group of transactions
def max_deposits(month, transactions):
    most_deposits = {'id': '', 'amount': 0}
    for user_id, user_transactions in transactions.iteritems():
        user_total = 0
        # Add up deposit amounts
        for user_transaction in user_transactions:
            user_total += user_transaction['amount']
        if user_total > most_deposits['amount']:
            most_deposits['id'] = user_id
            most_deposits['amount'] = user_total
    print month + " user who deposited the most is User " + most_deposits['id'] + \
          ", who deposited $" + str(most_deposits['amount'])


# Get user with most transactions per group of transactions
def max_transactions(month, transactions):
    most_transactions = {'id': '', 'amount': 0}
    for user_id, user_transactions in transactions.iteritems():
        user_total = len(user_transactions)
        if user_total > most_transactions['amount']:
            most_transactions['id'] = user_id
            most_transactions['amount'] = user_total

    print month + " user who made the most transactions is User " + most_transactions['id'] + \
          ", who made " + str(most_transactions['amount'])



# Script starts here and calls the functions above
users = load_users("StartingData.csv")
jan = load_transactions("Jan.csv")
feb = load_transactions("Feb.csv")
mar = load_transactions("Mar.csv")
months = [jan, feb, mar]

# Calculate penalties
for user in users:
    user_id = user['id']
    total_penalties = 0
    for month in months:
        total_penalties += calculate_penalties(user, month)
    user['penalties'] = total_penalties
# Output users with penalties
print 'Users that owe penalties:'
for user in users:
    if (user['penalties'] > 0):
        print user['id'] + ": $" + str(user['penalties'])

# Calculate and output user with the most deposits
max_deposits("January", months[0])
max_deposits("February", months[1])
max_deposits("March", months[2])
# Calculate and output user with the most transactions
max_transactions("January", months[0])
max_transactions("February", months[1])
max_transactions("March", months[2])
